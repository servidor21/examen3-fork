<?php /* Smarty version 3.1.27, created on 2016-03-02 17:19:54
         compiled from "template\pet.tpl" */ ?>
<?php
/*%%SmartyHeaderCode:3011556d712aa8f51a1_05667983%%*/
if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e13aa759d0dd2f91fdb433d344902e5fd01bf685' => 
    array (
      0 => 'template\\pet.tpl',
      1 => 1456935592,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3011556d712aa8f51a1_05667983',
  'variables' => 
  array (
    'language' => 0,
    'url' => 0,
    'lang' => 0,
    'rows' => 0,
    'row' => 0,
  ),
  'has_nocache_code' => false,
  'version' => '3.1.27',
  'unifunc' => 'content_56d712aa962f74_14395268',
),false);
/*/%%SmartyHeaderCode%%*/
if ($_valid && !is_callable('content_56d712aa962f74_14395268')) {
function content_56d712aa962f74_14395268 ($_smarty_tpl) {

$_smarty_tpl->properties['nocache_hash'] = '3011556d712aa8f51a1_05667983';
echo $_smarty_tpl->getSubTemplate ("template/header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"encabezado"), 0);
?>

<div id="content">
    <h2><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('pet_list');?>
</h2>
    
    <p><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/pet/add"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('new_pet');?>
</a></p>
    <p>Filtrar mascotas por nombre: <input type="text" id="filter"></p>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('name');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('species');?>
</th>

            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('birth_date');?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('operations');?>
</th>
        </tr>
        </thead>
        <tbody id="tbodyList">
        <?php
$_from = $_smarty_tpl->tpl_vars['rows']->value;
if (!is_array($_from) && !is_object($_from)) {
settype($_from, 'array');
}
$_smarty_tpl->tpl_vars['row'] = new Smarty_Variable;
$_smarty_tpl->tpl_vars['row']->_loop = false;
foreach ($_from as $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
$foreach_row_Sav = $_smarty_tpl->tpl_vars['row'];
?>
            <tr id="row<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
">
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['nombre'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['especie'];?>
</td>

                <td><?php echo $_smarty_tpl->tpl_vars['row']->value['fecha'];?>
</td>
                <td>

                    <a class="deleteRecord" id="delete<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
">borrar</a>   
                    <a class="petVisit" id="pet<?php echo $_smarty_tpl->tpl_vars['row']->value['id'];?>
">visita</a>   
                </td>
            </tr>
        <?php
$_smarty_tpl->tpl_vars['row'] = $foreach_row_Sav;
}
?>
        </tbody>
    </table>
    
    <p><a href="<?php echo $_smarty_tpl->tpl_vars['url']->value;
echo $_smarty_tpl->tpl_vars['lang']->value;?>
/pet/add"><?php echo $_smarty_tpl->tpl_vars['language']->value->translate('new_pet');?>
</a></p>

 </div>
<?php echo $_smarty_tpl->getSubTemplate ("template/footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('title'=>"footer"), 0);

}
}
?>